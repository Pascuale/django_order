from rest_framework import serializers
from .models import (
    CenterDistributtion, Store, AssociateCompany,
    Item, Line, Customer, Provider)
from django.conf import settings
from django.contrib.auth.models import User
AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')


class LineSerializer(serializers.ModelSerializer):
    item = serializers.SerializerMethodField()

    class Meta:
        model = Line
        fields = [
            'item', 'quantity', 'amount'
        ]

    def get_item(self, instance):
        return ItemSerializer(instance.item).data


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = [
            'code', 'description'
        ]


class ProviderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Provider
        fields = [
            'name', 'address'
        ]


class CustomerSerializer(serializers.ModelSerializer):
    first_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()

    class Meta:
        model = Customer
        fields = [
            'id', 'first_name', 'last_name', 'code',
            'picture', 'address', 'type'
        ]

    def get_first_name(self, instance):
        return instance.user.first_name

    def get_last_name(self, instance):
        return instance.user.last_name


class CenterDistributtionSerializer(serializers.ModelSerializer):
    customer = CustomerSerializer(read_only=True)
    lines = LineSerializer(many=True)

    class Meta:
        model = CenterDistributtion
        fields = [
            'id', 'number', 'customer', 'is_express',
            'lines', 'date_created', 'reference'
            ]


class CenterDistributtionCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = CenterDistributtion
        fields = [
            'id', 'number', 'customer', 'is_express', 'lines',
            'date_created', 'reference'
        ]


class StoreSerializer(serializers.ModelSerializer):
    customer = CustomerSerializer(read_only=True)
    lines = LineSerializer(many=True)

    class Meta:
        model = Store
        fields = [
            'id', 'number', 'is_express', 'lines',
            'reference', 'code', 'customer'
        ]


class StoreCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Store
        fields = [
            'id', 'number', 'is_express', 'lines',
            'reference', 'code', 'customer'
        ]


class AssociateCompanySerializer(serializers.ModelSerializer):
    customer = CustomerSerializer(read_only=True)
    lines = LineSerializer(many=True)

    class Meta:
        model = AssociateCompany
        fields = [
            'id', 'number', 'is_express', 'lines',
            'reference', 'code', 'order_details',
            'customer', 'date_created'
        ]


class AssociateCompanyCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = AssociateCompany
        fields = [
            'id', 'number', 'is_express', 'lines',
            'reference', 'code', 'order_details',
            'customer', 'date_created'
        ]
