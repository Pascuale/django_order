# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from .models import CenterDistributtion, Store, AssociateCompany
from .serializers import (
    CenterDistributtionSerializer,
    StoreSerializer, AssociateCompanySerializer,
    CenterDistributtionCreateSerializer, StoreCreateSerializer,
    AssociateCompanyCreateSerializer)
from rest_framework import filters, generics
from filters.mixins import FiltersMixin
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from django.utils.translation import ugettext_lazy as _


is_express_param = openapi.Parameter(
    'is_express', in_=openapi.IN_QUERY,
    description=_('Is express ?'), type=openapi.TYPE_STRING,
    enum=['True', 'False']
)

class CenterDistributtionList(FiltersMixin, generics.ListAPIView):
    queryset = CenterDistributtion.objects.prefetch_related('lines') \
        .select_related('customer').all()
    serializer_class = CenterDistributtionSerializer
    ordering_fields = ('id', 'date_created', 'customer_type', 'number')
    filter_mappings = {
        'is_express': 'is_express',
        'customer_type': 'customer__type',
        'date_created': 'date_created',
        'date_created__gte': 'date_created__gte',
        'date_created__lte': 'date_created__lte',
    }
    filter_backends = (filters.OrderingFilter,)
    filter_fields = ('is_express', 'customer_type')
    date_created_gte_param = openapi.Parameter(
        'date_created_gte', in_=openapi.IN_QUERY,
        description=_('date created greater than or equal to.'),
        type=openapi.FORMAT_DATETIME
    )
    date_created_lte_param = openapi.Parameter(
        'date_created_lte', in_=openapi.IN_QUERY,
        description=_('date created less than or equal to.'),
        type=openapi.FORMAT_DATETIME
    )
    date_created_param = openapi.Parameter(
        'date_created', in_=openapi.IN_QUERY,
        description=_('date created'), type=openapi.FORMAT_DATETIME
    )
    # is_express_param = openapi.Parameter(
    #     'is_express', in_=openapi.IN_QUERY,
    #     description=_('Is express ?'), type=openapi.TYPE_BOOLEAN
    # )
    customer_type_param = openapi.Parameter(
        'customer_type', in_=openapi.IN_QUERY,
        description=_('Customer type'), type=openapi.TYPE_STRING,
        enum=['normal', 'silver', 'gold', 'platinum']
    )

    @swagger_auto_schema(
        responses={200: CenterDistributtionSerializer},
        operation_description="GET api/center-distributtion/",
        manual_parameters=[
            is_express_param, customer_type_param, date_created_param,
            date_created_gte_param, date_created_lte_param])
    def get(self, request, *args, **kwargs):
        """
        Concrete view for listing a queryset.
        """
        return self.list(request, *args, **kwargs)


class CenterDistributtionCreate(generics.CreateAPIView):
    queryset = CenterDistributtion.objects.all()
    serializer_class = CenterDistributtionCreateSerializer


class StoreList(FiltersMixin, generics.ListAPIView):
    queryset = Store.objects.prefetch_related('lines') \
        .select_related('customer').all()
    serializer_class = StoreSerializer
    ordering_fields = ('id', 'date_created', 'customer_type', 'number')
    filter_mappings = {
        'is_express': 'is_express',
        'customer_type': 'customer__type',
        'date_created': 'date_created',
        'date_created__gte': 'date_created__gte',
        'date_created__lte': 'date_created__lte',
    }
    filter_backends = (filters.OrderingFilter,)
    filter_fields = ('is_express', 'customer_type')
    date_created_gte_param = openapi.Parameter(
        'date_created_gte', in_=openapi.IN_QUERY,
        description=_('date created greater than or equal to.'),
        type=openapi.FORMAT_DATETIME
    )
    date_created_lte_param = openapi.Parameter(
        'date_created_lte', in_=openapi.IN_QUERY,
        description=_('date created less than or equal to.'),
        type=openapi.FORMAT_DATETIME
    )
    date_created_param = openapi.Parameter(
        'date_created', in_=openapi.IN_QUERY,
        description=_('date created'), type=openapi.FORMAT_DATETIME
    )
    # is_express_param = openapi.Parameter(
    #     'is_express', in_=openapi.IN_QUERY,
    #     description=_('Is express ?'), type=openapi.TYPE_BOOLEAN
    # )
    customer_type_param = openapi.Parameter(
        'customer_type', in_=openapi.IN_QUERY,
        description=_('Customer type'), type=openapi.TYPE_STRING,
        enum=['normal', 'silver', 'gold', 'platinum']
    )

    @swagger_auto_schema(
        responses={200: StoreSerializer},
        operation_description="GET api/store/",
        manual_parameters=[
            is_express_param, customer_type_param, date_created_param,
            date_created_gte_param, date_created_lte_param])
    def get(self, request, *args, **kwargs):
        """
        Concrete view for listing a queryset.
        """
        return self.list(request, *args, **kwargs)


class StoreCreate(generics.CreateAPIView):
    queryset = Store.objects.all()
    serializer_class = StoreCreateSerializer


class AssociateCompanyList(FiltersMixin, generics.ListAPIView):
    queryset = AssociateCompany.objects.prefetch_related('lines') \
        .select_related('customer').all()
    serializer_class = AssociateCompanySerializer
    ordering_fields = ('id', 'date_created', 'customer_type', 'number')
    filter_mappings = {
        'is_express': 'is_express',
        'customer_type': 'customer__type',
        'date_created': 'date_created',
        'date_created__gte': 'date_created__gte',
        'date_created__lte': 'date_created__lte',
    }
    filter_backends = (filters.OrderingFilter,)
    filter_fields = ('is_express', 'customer_type')
    date_created_gte_param = openapi.Parameter(
        'date_created_gte', in_=openapi.IN_QUERY,
        description=_('date created greater than or equal to.'),
        type=openapi.FORMAT_DATETIME
    )
    date_created_lte_param = openapi.Parameter(
        'date_created_lte', in_=openapi.IN_QUERY,
        description=_('date created less than or equal to.'),
        type=openapi.FORMAT_DATETIME
    )
    date_created_param = openapi.Parameter(
        'date_created', in_=openapi.IN_QUERY,
        description=_('date created'), type=openapi.FORMAT_DATETIME
    )
    customer_type_param = openapi.Parameter(
        'customer_type', in_=openapi.IN_QUERY,
        description=_('Customer type'), type=openapi.TYPE_STRING,
        enum=['normal', 'silver', 'gold', 'platinum']
    )

    @swagger_auto_schema(
        responses={200: StoreSerializer},
        operation_description="GET api/associate-company/",
        manual_parameters=[
            is_express_param, customer_type_param, date_created_param,
            date_created_gte_param, date_created_lte_param])
    def get(self, request, *args, **kwargs):
        """
        Concrete view for listing a queryset.
        """
        return self.list(request, *args, **kwargs)


class AssociateCompanyCreate(generics.CreateAPIView):
    queryset = AssociateCompany.objects.all()
    serializer_class = AssociateCompanyCreateSerializer
