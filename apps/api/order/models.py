# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from .abstract_models import AbstractOrder


class Provider(models.Model):
    name = models.CharField(max_length=128)
    address = models.CharField(max_length=255)

    class Meta:
        app_label = 'order'
        ordering = ['name']
        unique_together = ('name',)
        verbose_name = _('Provider')
        verbose_name_plural = _('Providers')

    def __str__(self):
        return F'{self.name}-{self.address}'


class Customer(models.Model):
    NORMAL, SILVER, GOLD, PLATINUM = 'normal', 'silver', 'gold', 'platinum'
    CUSTOMER_CHOICES = (
        (NORMAL, _('Normal')),
        (SILVER, _('Silver')),
        (GOLD, _('Gold')),
        (PLATINUM, _('Platinum'))
    )
    AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')
    user = models.ForeignKey(
        AUTH_USER_MODEL, related_name='user',
        verbose_name=_("User"), on_delete=models.CASCADE)
    code = models.CharField(max_length=128)
    picture = models.ImageField(upload_to='customers')
    address = models.CharField(max_length=255)
    type = models.CharField(max_length=10, choices=CUSTOMER_CHOICES)

    @property
    def display_name(self):
        return self.user.first_name or self.user.username

    class Meta:
        app_label = 'order'
        ordering = ['type']
        unique_together = ('type', 'code')
        verbose_name = _('Customer')
        verbose_name_plural = _('Customers')

    def __str__(self):
        return self.display_name


class Item(models.Model):
    code = models.PositiveBigIntegerField()
    description = models.TextField(_('Description'))
    price_excl_tax = models.DecimalField(
        _("Price (excl. tax)"), decimal_places=2, max_digits=12,
        blank=True, null=True)
    providers = models.ManyToManyField(
        'order.Provider', through='ItemProvider',
        verbose_name=_("providers"))

    class Meta:
        app_label = 'order'
        ordering = ['code']
        unique_together = ('code',)
        verbose_name = _('Item')
        verbose_name_plural = _('Items')

    def __str__(self):
        return F'{self.code}-{self.description}-${self.price_excl_tax}'


class ItemProvider(models.Model):
    """
    Joining model between items and providers. Exists to allow customising.
    """
    item = models.ForeignKey(
        'order.Item',
        on_delete=models.CASCADE,
        verbose_name=_("Item"))
    provider = models.ForeignKey(
        'order.Provider',
        on_delete=models.CASCADE,
        verbose_name=_("Provider"))

    class Meta:
        app_label = 'order'
        ordering = ['item', 'provider']
        unique_together = ('item', 'provider')
        verbose_name = _('Item provider')
        verbose_name_plural = _('Item providers')

    def __str__(self):
        return F"""<Item provider for item '{self.item.code}'
                - provider '{self.provider.name}'>"""


class Line(models.Model):
    item = models.ForeignKey(
        'order.Item',
        on_delete=models.CASCADE,
        related_name='item_lines',
        verbose_name=_("Item"))
    quantity = models.PositiveIntegerField(_('Quantity'), default=1)
    amount = models.DecimalField(
        _("Amount"), decimal_places=2, max_digits=12)

    def __str__(self):
        return F'{self.item.code}'


class Detail(models.Model):
    description = models.TextField()

    def __str__(self):
        return F'{self.description}'


class CenterDistributtion(AbstractOrder):
    reference = models.CharField(max_length=128)

    class Meta:
        app_label = 'order'
        # Enforce sorting in order of creation
        ordering = ['pk']
        verbose_name = _('Distribution Center Order')
        verbose_name_plural = _('Distribution Center Orders')


class Store(AbstractOrder):
    reference = models.CharField(max_length=128)
    code = models.PositiveBigIntegerField()

    class Meta:
        app_label = 'order'
        # Enforce sorting in order of creation
        ordering = ['pk']
        verbose_name = 'Store Order'
        verbose_name_plural = 'Store Orders'


class AssociateCompany(AbstractOrder):
    reference = models.CharField(max_length=128)
    code = models.PositiveBigIntegerField()
    order_details = models.ManyToManyField(
        'order.Detail')

    class Meta:
        app_label = 'order'
        # Enforce sorting in order of creation
        ordering = ['pk']
        verbose_name = 'Associate Company Order'
        verbose_name_plural = _('Associate Company Orders')
