from django.urls import path, re_path
from .views import (
    CenterDistributtionList, StoreList, AssociateCompanyList,
    CenterDistributtionCreate, StoreCreate, AssociateCompanyCreate)
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from django.utils.translation import ugettext_lazy as _

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

schema_view = get_schema_view(
    openapi.Info(
        title=_('Orders API'),
        default_version='v1',
        description=_(
            "Api to manage the information of an electronic"
            "products company, mostly the generation of orders and"
            "consultation of the same."),
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="ricardopascual.developer@gmail.com"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path(
        'api/token/', TokenObtainPairView.as_view(),
        name='token_obtain_pair'),
    path(
        'api/token/refresh/', TokenRefreshView.as_view(),
        name='token_refresh'),
    path(
        'api/center-distribution/', CenterDistributtionList.as_view(),
        name='api_orders_center_distribution'),
    path(
        'api/center-distribution/create/', CenterDistributtionCreate.as_view(),
        name='api_orders_center_distribution_create'),
    path(
        'api/store/', StoreList.as_view(),
        name='api_orders_store'),
    path(
        'api/store/create', StoreCreate.as_view(),
        name='api_orders_store_create'),
    path(
        'api/associate-company/', AssociateCompanyList.as_view(),
        name='api_orders_associate_company'),
    path(
        'api/associate-company/create', AssociateCompanyCreate.as_view(),
        name='api_orders_associate_company_create'),
    re_path(
        r'^swagger(?P<format>\.json|\.yaml)$',
        schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path(
        '', schema_view.with_ui('swagger', cache_timeout=0),
        name='schema-swagger-ui'),
    re_path(
        r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0),
        name='schema-redoc'),
]