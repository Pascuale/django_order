from django.db import models
from django.utils.translation import gettext_lazy as _


class AbstractOrder(models.Model):
    customer = models.ForeignKey(
        'order.Customer', on_delete=models.CASCADE)
    number = models.PositiveBigIntegerField()
    date_created = models.DateTimeField(
        _("Date created"), auto_now_add=True, db_index=True)
    is_express = models.BooleanField(default=False)
    lines = models.ManyToManyField('order.Line')
    date_dispatch = models.DateTimeField(_("Date dispatch"))

    class Meta:
        app_label = 'orders'
        abstract = True
