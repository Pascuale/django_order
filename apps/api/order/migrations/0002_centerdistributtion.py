# Generated by Django 3.1.3 on 2020-12-06 20:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CenterDistributtion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.PositiveBigIntegerField()),
                ('date_created', models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Date created')),
                ('is_express', models.BooleanField(default=False)),
                ('date_dispatch', models.DateTimeField(verbose_name='Date dispatch')),
                ('reference', models.CharField(max_length=128)),
                ('customer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='order.customer')),
                ('lines', models.ManyToManyField(to='order.Line')),
            ],
            options={
                'verbose_name': 'Distribution Center Order',
                'verbose_name_plural': 'Distribution Center Orders',
                'ordering': ['pk'],
            },
        ),
    ]
