# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import (
    Customer, Item, Provider, Line, Detail, CenterDistributtion,
    Store, AssociateCompany, ItemProvider)

admin.site.register(Customer)
admin.site.register(Item)
admin.site.register(Provider)
admin.site.register(Line)
admin.site.register(Detail)
admin.site.register(CenterDistributtion)
admin.site.register(Store)
admin.site.register(AssociateCompany)
admin.site.register(ItemProvider)
