django_order
=======================

**Order management** This project was developed in Django.


Status:
    in development

Django Version:
    3.1.3


Documentation
-------------

# Docker image for project
## Build the images and run the containers:
1. Building for ***local development***
    ```sh
    $ CURRENT_UID=$(id -u):$(id -g) docker-compose build --build-arg user_id=$(id -u) --build-arg group_id=$(id -g)
    ```
    Runing with log output in console
    ```sh
    $ CURRENT_UID=$(id -u):$(id -g) docker-compose up
    ```
Basic information is loaded when lifting the containers


## Models
At the models level, an abstract model was generated for the types of orders,
this to avoid null or empty fields within the database, however another
approach that is also valid is to create a single model only adding validation
when generating the orders, however this it causes the generation of more
rules and therefore it would be necessary to validate more scenarios than
what was proposed.

## Serializers
Two serializers were created, one to show and the other to create the orders,
this in order that in the serializer to show the purchase orders they are
easier to implement for the frontend since otherwise more requests would
have to be made to achieve obtaining information this would make the
implementation complex.

## Test
I could not do any type of test due to lack of time.

## Project structure
In the project structure, a template was used in order to make the
project easy to create functionalities and have a better handling at the
time of maintenance in case the project grows.

## Authentication
Basic authentication was added with jwt to achieve a functional
application and that any front could consume the information, at some
point oauth could be integrated for a more robust integration.

Important: It is currently disabled.


To enable uncomment the lines in settings/common.py
```
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated',
    ],
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework_simplejwt.authentication.JWTAuthentication',
    ],
```
Request POST
`URL https://127.0.0.1:8000/api/token/`

Body

```
{
  "username": "admin",
  "password": "admin09"
}
```


Response example

HTTP 200 OK
Allow: POST, OPTIONS
Content-Type: application/json
Vary: Accept

```
{
"refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTYwNzUzMzM0OSwianRpIjoiZTNhYmU0OTk2ZTk3NDY3ZDhhYWVmNTJmOWI4MzNkODEiLCJ1c2VyX2lkIjoxfQ.wJuf62x_qaW1vUgw__8buHj2OcJ9dKy21mVLCxBkD7U",
"access": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjA3NDQ3MjQ5LCJqdGkiOiI2MTdhNzMwMGQ1MWI0NmQ1YjFhN2I5MDBlNjZkNmYxYiIsInVzZXJfaWQiOjF9.ZKS_gVj8RIbFcuyDFO17YJPBNmt054AWGznuvwDApWU"
}
```


## Add jwt in header

`Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjA3NDQ3MjQ5LCJqdGkiOiI2MTdhNzMwMGQ1MWI0NmQ1YjFhN2I5MDBlNjZkNmYxYiIsInVzZXJfaWQiOjF9.ZKS_gVj8RIbFcuyDFO17YJPBNmt054AWGznuvwDApWU`


## Debug
To debug the applications I like to use **pudb** and **debug_toolbar**.

## Docs Swagger

https://127.0.0.1:8000/

## Diagram BD 


## Filter in models
Drf-url-filters was added for generating filters in a clean,
simple and configurable way.


## Creation of order of type Distribution Center
https://127.0.0.1:8000/api/center-distribution/create/

## Creation of order of type Associate Company
https://127.0.0.1:8000/api/associate-company/create

## Creation of order of type store
https://127.0.0.1:8000/api/store/create/

## Get rush orders from Platinum Customer Distribution Centers
https://127.0.0.1:8000/api/center-distribution/?express=True&customer_type=platinum

