# pull official base image
FROM python:3.6.8-alpine3.9

# set environment varibles
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# user, group varibles
ARG group_id=1006
ARG user_id=1000
RUN echo "Build image with user: ${user_id} and group: ${group_id}"
RUN addgroup -g $group_id -S appuser && adduser -u $user_id -S appuser -G appuser
# set work directory
WORKDIR /usr/src/django_order

# install psycopg2
RUN apk update \
    && apk --no-cache add --virtual build-deps binutils gcc python3-dev musl-dev postgresql-dev graphviz-dev\
    && apk add  build-base ttf-ubuntu-font-family fontconfig\
    # wget dependency
    openssl \
    libffi-dev \
    openssl-dev \
    # Pillow dependencies
    jpeg-dev \
    zlib-dev \
    freetype-dev \
    lcms2-dev \
    openjpeg-dev \
    tiff-dev \
    libpq \
    py-pygraphviz \
    gettext \
    libxslt-dev \
    git
RUN pip install --upgrade pip \
    && pip install pipenv

# install python library
ADD requirements/development.txt /tmp/development.txt
RUN pip install -r /tmp/development.txt

# del dev libs
RUN apk del  build-deps binutils gcc python3-dev musl-dev postgresql-dev libffi-dev openssl-dev

# copy entrypoint.sh
COPY ./entrypoint.sh /usr/src/django_order/entrypoint.sh
# copy project
ADD . /usr/src/django_order

USER appuser

ENTRYPOINT ["/usr/src/django_order/entrypoint.sh"]